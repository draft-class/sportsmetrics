package baseball

import (
	"fmt"
	"strings"
	"math"
)

// Reference: https://www.mlb.com/glossary/standard-stats/batting-average

// BattingAverage calculates an average of hits per at-bats
func BattingAverage(hits uint64, atBats uint64) float64 {
	avg := float64(hits) / float64(atBats)

	return math.Round(avg * 1000) / 1000
}

// BattingAverageString calculates an average of hits per at-bats
// and outputs a string in the normal batting average format
func BattingAverageString(hits uint64, atBats uint64) string {
	avg := BattingAverage(hits, atBats)

	return strings.TrimLeft(fmt.Sprintf("%.3f", avg), "0")
}
