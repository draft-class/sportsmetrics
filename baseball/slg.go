package baseball

import (
	"fmt"
	"math"
	"strings"
)
// Reference: https://www.mlb.com/glossary/standard-stats/slugging-percentage

// SluggingPercentage calculates the batters slugging percentage
func SluggingPercentage(single uint, double uint, triple uint, homerun uint, atbats uint) float64 {
	output := single

	output += (double * 2)
	output += (triple * 3)
	output += (homerun * 4)

	outputFloat := float64(output) / float64(atbats)

	return math.Round(outputFloat * 1000) / 1000
}

// SluggingPercentageString calculates slugging percentage
// and outputs a string in the normal slugging format
func SluggingPercentageString(single uint, double uint, triple uint, homerun uint, atbats uint) string {
	slg := SluggingPercentage(single, double, triple, homerun, atbats)

	return strings.TrimLeft(fmt.Sprintf("%.3f", slg), "0")
}
