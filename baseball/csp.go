package baseball

import (
	"fmt"
	"math"
)

// Reference: https://www.mlb.com/glossary/standard-stats/caught-stealing-percentage

// CaughtStrealingPercentage is the percent of attempted stealers thrown out
func CaughtStrealingPercentage(catcherCaughtStealing uint64, StolenBaseAttempts uint64) float64 {
	csp := float64(catcherCaughtStealing) / float64(StolenBaseAttempts)

	return math.Round(csp * 100) / 100
}

// CaughtStrealingPercentageString is caught stealing in a traditional format
func CaughtStrealingPercentageString(catcherCaughtStealing uint64, StolenBaseAttempts uint64) string {
	csp := CaughtStrealingPercentage(catcherCaughtStealing, StolenBaseAttempts)

	return fmt.Sprintf("%.0f%%", csp * 100)
}
