package baseball

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestBattingAverage(t *testing.T) {
	tests := []struct {
		Hit    uint64
		AtBat  uint64
		Output float64
	}{
		{ Hit: 1, AtBat: 3, Output: 0.333 },
		// Willie Mays
		{ Hit: 3283, AtBat: 10881, Output: 0.302 },
		// Pete Rose
		{ Hit: 4256, AtBat: 14053, Output: 0.303 },
		// Ted Williams
		{ Hit: 2654, AtBat: 7706, Output: 0.344 },
	}
	for _, test := range tests {
		avg := BattingAverage(test.Hit, test.AtBat)
		
		assert.Equal(t, test.Output, avg)
	}
}

func TestBattingAverageString(t *testing.T) {
	tests := []struct {
		Hit    uint64
		AtBat  uint64
		Output string
	}{
		{ Hit: 1, AtBat: 3, Output: ".333" },
		// Willie Mays
		{ Hit: 3283, AtBat: 10881, Output: ".302" },
		// Pete Rose
		{ Hit: 4256, AtBat: 14053, Output: ".303" },
		// Ted Williams
		{ Hit: 2654, AtBat: 7706, Output: ".344" },
	}
	for _, test := range tests {
		avg := BattingAverageString(test.Hit, test.AtBat)
		
		assert.Equal(t, test.Output, avg)
	}
}
