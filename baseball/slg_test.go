package baseball

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSlugginPercentage(t *testing.T) {
	tests := []struct {
		Single  uint
		Double  uint
		Triple  uint
		Homerun uint
		AtBat   uint
		Output  float64
	}{
		// Willie Mays
		{ Single: 1960, Double: 523, Triple: 140, Homerun: 660, AtBat: 10881, Output: 0.557 },
		// Babe Ruth
		{ Single: 1517, Double: 506, Triple: 136, Homerun: 714, AtBat: 8399, Output: 0.690 },
		// Hank Aaron
		{ Single: 2294, Double: 624, Triple: 98, Homerun: 755, AtBat: 12364, Output: 0.555 },
	}
	for _, test := range tests {
		avg := SluggingPercentage(test.Single, test.Double, test.Triple, test.Homerun, test.AtBat)
		
		assert.Equal(t, test.Output, avg)
	}
}

func TestSlugginPercentageString(t *testing.T) {
	tests := []struct {
		Single  uint
		Double  uint
		Triple  uint
		Homerun uint
		AtBat   uint
		Output  string
	}{
		// Willie Mays
		{ Single: 1960, Double: 523, Triple: 140, Homerun: 660, AtBat: 10881, Output: ".557" },
		// Babe Ruth
		{ Single: 1517, Double: 506, Triple: 136, Homerun: 714, AtBat: 8399, Output: ".690" },
		// Hank Aaron
		{ Single: 2294, Double: 624, Triple: 98, Homerun: 755, AtBat: 12364, Output: ".555" },
	}
	for _, test := range tests {
		avg := SluggingPercentageString(test.Single, test.Double, test.Triple, test.Homerun, test.AtBat)
		
		assert.Equal(t, test.Output, avg)
	}
}
