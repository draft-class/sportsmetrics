package baseball

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestCaughtStealingPercentage(t *testing.T) {
	tests := []struct {
		CaughtStealing      uint64
		StolenBaseAttempts  uint64
		Output              float64
	}{
		{ CaughtStealing: 1, StolenBaseAttempts: 3, Output: 0.33 },
		// Johnny Bench
		{ CaughtStealing: 469, StolenBaseAttempts: 1079, Output: 0.43 },
		// Yogi Berra
		{ CaughtStealing: 403, StolenBaseAttempts: 829, Output: 0.49 },
		// Carlton Fisk
		{ CaughtStealing: 665, StolenBaseAttempts: 1967, Output: 0.34 },
	}
	for _, test := range tests {
		avg := CaughtStrealingPercentage(test.CaughtStealing, test.StolenBaseAttempts)
		
		assert.Equal(t, test.Output, avg)
	}
}

func TestCaughtStealingPercentageString(t *testing.T) {
	tests := []struct {
		CaughtStealing      uint64
		StolenBaseAttempts  uint64
		Output              string
	}{
		{ CaughtStealing: 1, StolenBaseAttempts: 3, Output: "33%" },
		// Johnny Bench
		{ CaughtStealing: 469, StolenBaseAttempts: 1079, Output: "43%" },
		// Yogi Berra
		{ CaughtStealing: 403, StolenBaseAttempts: 829, Output: "49%" },
		// Carlton Fisk
		{ CaughtStealing: 665, StolenBaseAttempts: 1967, Output: "34%" },
	}
	for _, test := range tests {
		avg := CaughtStrealingPercentageString(test.CaughtStealing, test.StolenBaseAttempts)
		
		assert.Equal(t, test.Output, avg)
	}
}
